from fabric.api import local

def build_docker():
    local('docker build -t registry.gitlab.com/stenius/powerbowl .')
    local('docker push registry.gitlab.com/stenius/powerbowl')
