#!/bin/bash

# wait for postgres container to come up
until nc -z postgres 5432
do
    echo "waiting for postgres container..."
    sleep 0.5
done

# Apply database migrations
echo "Apply database migrations"
python /app/manage.py migrate

# Start server
echo "Starting server"
python /app/manage.py runserver 0.0.0.0:8000
