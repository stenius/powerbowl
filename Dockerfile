FROM python:3.4

RUN apt-get update && apt-get install -y netcat

ADD requirements.txt /tmp

RUN pip3 install -r /tmp/requirements.txt

ADD docker-entrypoint.sh /app/docker-entrypoint.sh

ADD powerbowl /app

EXPOSE 8000
CMD ["python", "/app/manage.py", "runserver"]
