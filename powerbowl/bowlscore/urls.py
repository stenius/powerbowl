from django.conf.urls import url, include

from rest_framework import routers

from bowlscore import views

router = routers.DefaultRouter()
router.register(r'games', views.GameViewSet)
router.register(r'bowler', views.BowlerViewSet)
router.register(r'bowlerscores', views.ScoreViewSet)

# Wire up our API using automatic URL routing.
urlpatterns = [
    url(r'^', include(router.urls)),
]
