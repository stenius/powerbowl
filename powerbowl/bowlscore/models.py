from __future__ import unicode_literals

from django.db import models
from django.contrib.postgres.fields import ArrayField

class Bowler(models.Model):
    pass


class Game(models.Model):
    bowlers = models.ManyToManyField('Bowler', through='BowlerScores')


class BowlerScores(models.Model):
    '''holds score information for a single player in a single game'''
    game = models.ForeignKey('Game')
    bowler = models.ForeignKey('Bowler')
    bowls = ArrayField(models.PositiveSmallIntegerField())
    # bowls are a list of the pin counts knocked down every time the ball is
    # thrown
    #
    # If 2 strikes and a 5/ is bowled, the list would be 10, 10, 5, 5

    def mark_bowl_score(self, score):
        self.bowls.append(score)
        self.save()

    def get_frame_tuples(self, show_open_frames=False):
        '''returns the'''
        scores = []
        frame_open = None # open frames occur during a strike or spare
        current_frame = []

        for index, score in enumerate(self.bowls):
            # if it is a strike return the score and a "skip" for the 2nd bowl
            if score == 10:
                current_frame = [score, None]
                if show_open_frames:
                    frame_open = False
                    scores.append((current_frame, frame_open))
                else:
                    scores.append(current_frame)
                current_frame = []
                continue

            current_frame.append(score)
            # if it is the second bowl in a frame reset the current_frame
            # object
            if len(current_frame) == 2:
                if show_open_frames:
                    if current_frame[0] + score == 10:
                        frame_open = False
                    else:
                        frame_open = True
                    scores.append((current_frame, frame_open))
                else:
                        scores.append(current_frame)
                current_frame = []
                continue
        # make sure the last item gets added if the current_frame is not done
        else:
            if current_frame:
                if show_open_frames:
                    frame_open = None
                    scores.append((current_frame, frame_open))
                else:
                    scores.append(current_frame)

        return scores

    def get_score(self):
        is_valid = True # track validity of score in cases where the frame is still open
        score = 0 # running total of current score

        frame_tuples = self.get_frame_tuples(show_open_frames=True)

        for index, (frame, frame_open) in enumerate(frame_tuples):
            # if this is not a fill ball frame
            if index + 1 <= 10:
                score += frame[0]
                # if a strike was bowled, check the next 2 bowls
                if frame[0] == 10:
                    # try to get the next frame if it exists
                    try:
                        next_frame = frame_tuples[index + 1][0]
                    except IndexError:
                        next_frame = None

                    if not next_frame:
                        is_valid = False
                    else:
                        score += next_frame[0]

                        # if the next frame was also a strike, get one more frame
                        if next_frame[0] == 10:
                            try:
                                next_next_frame = frame_tuples[index + 2][0]
                            except IndexError:
                                next_next_frame = None

                            if not next_next_frame:
                                is_valid = False
                            else:
                                score += next_next_frame[0]
                        # if next frame is not a strike, just add the 2nd bowl from
                        # that frame
                        else:
                            try:
                                score += next_frame[1]
                            except IndexError:
                                is_valid = False

                # if a strike was not bowled in the frame, add the 2nd bowl to the
                # score
                else:
                    try:
                        last_frame_bowl = frame[1]
                    except IndexError:
                        last_frame_bowl = 0
                    score += last_frame_bowl

                    # if this frame is a spare, get the next bowl from the next
                    # frame
                    if frame[0] + last_frame_bowl == 10:
                        try:
                            next_frame = frame_tuples[index + 1][0]
                        except IndexError:
                            next_frame = None

                        if not next_frame:
                            is_valid = False
                        else:
                            score += next_frame[0]

        return score, is_valid
