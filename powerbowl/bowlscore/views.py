from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from .models import Game, Bowler, BowlerScores
from .serializers import GameSerializer, BowlerSerializer, ScoreSerializer


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows games to be viewed or created.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class BowlerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows bowlers to be viewed or edited.
    """
    queryset = Bowler.objects.all()
    serializer_class = BowlerSerializer

class ScoreViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows scores to be viewed or edited.
    """
    queryset = BowlerScores.objects.all()
    serializer_class = ScoreSerializer

    @detail_route(methods=['post'], permission_classes=[],
            url_path='bowl/add')
    def add_score(self, request, pk=None):
        score = BowlerScores.objects.get(pk=pk).mark_bowl_score(request.data['pins'])
        return Response(score)
