from rest_framework import serializers

from .models import Game, Bowler, BowlerScores


class ScoreSerializer(serializers.ModelSerializer):
    frames = serializers.ReadOnlyField(source='get_frame_tuples')
    score = serializers.SerializerMethodField()
    score_is_valid = serializers.SerializerMethodField()

    class Meta:
        model = BowlerScores
        fields = ('bowler', 'score', 'score_is_valid', 'frames', 'url')

    def get_score(self, obj):
        return obj.get_score()[0]

    def get_score_is_valid(self, obj):
        return obj.get_score()[1]


class GameSerializer(serializers.HyperlinkedModelSerializer):
    bowlers = ScoreSerializer(source='bowlerscores_set', many=True,)

    class Meta:
        model = Game
        fields = ('id', 'url', 'bowlers')

    def create(self, validated_data):
        '''create a new game and add specified bowlers'''

        game = Game.objects.create()

        for bowler in validated_data['bowlerscores_set']:
            bowler_obj = Bowler.objects.get(id=bowler['bowler'].id)
            BowlerScores.objects.create(bowler=bowler_obj, game=game, bowls=[])

        return game


class BowlerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bowler
        fields = ('id', 'url',)
