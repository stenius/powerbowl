from django.test import TestCase

from .models import Bowler, BowlerScores, Game


class BowlingGameTestCase(TestCase):
    def setUp(self):
        bowler1 = Bowler.objects.create()
        bowler2 = Bowler.objects.create()
        game1 = Game.objects.create()
        bowler1_score = BowlerScores.objects.create(game=game1, bowler=bowler1,
                bowls=[])
        bowler2_score = BowlerScores.objects.create(game=game1, bowler=bowler2,
                bowls=[])

    def test_bowlers_in_game(self):
        '''bowlers added to game correctly'''
        game = Game.objects.get()

        self.assertEqual(game.bowlers.count(), 2)

    def test_add_score_incomplete_frame(self):
        '''bowlers score accounted for in incomplete frame'''
        bowler1_score = BowlerScores.objects.first()

        # make sure there is 1 item returned in the last tuple when frame is
        # open
        bowler1_score.mark_bowl_score(1)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(len(scores[-1][0]), 1)

        # after that frame is closed, make sure the length is 2
        bowler1_score.mark_bowl_score(2)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(len(scores[-1][0]), 2)

        # make sure the length is 2 when a strike is bowled
        bowler1_score.mark_bowl_score(10)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(len(scores[-1][0]), 2)

        # ensure the length is 1 after a stike is bowled and the frame is open
        bowler1_score.mark_bowl_score(5)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(len(scores[-1][0]), 1)

    def test_open_frames(self):
        '''frames are open and closed correctly'''
        bowler1_score = BowlerScores.objects.first()

        bowler1_score.mark_bowl_score(1)
        # bowled a incomplete frame
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(scores[0][1], None)

        # bowled a open frame with score 2
        bowler1_score.mark_bowl_score(1)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(scores[0][1], True)

        # bowled a strike
        bowler1_score.mark_bowl_score(10)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(scores[1][1], False)

        # bowled a 1/9 spare
        bowler1_score.mark_bowl_score(1)
        bowler1_score.mark_bowl_score(9)
        scores = bowler1_score.get_frame_tuples(show_open_frames=True)
        self.assertEqual(scores[2][1], False)

    def test_strike_scoring(self):
        '''score strike and next 2 bowls'''
        bowler1_score = BowlerScores.objects.first()

        bowler1_score.mark_bowl_score(10)
        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 10)
        self.assertEqual(is_valid, False)

        bowler1_score.mark_bowl_score(9)
        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 28)
        self.assertEqual(is_valid, False)

        bowler1_score.mark_bowl_score(0)
        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 28)
        self.assertEqual(is_valid, True)

    def test_spare_scoring(self):
        '''score spare and next bowl'''
        bowler1_score = BowlerScores.objects.first()

        bowler1_score.mark_bowl_score(5)
        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 5)
        self.assertEqual(is_valid, True)

        bowler1_score.mark_bowl_score(5)
        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 10)
        self.assertEqual(is_valid, False)

        bowler1_score.mark_bowl_score(1)
        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 12)
        self.assertEqual(is_valid, True)

    def test_spare_fill_ball_score(self):
        '''score after bowler throws a spare in the final frame'''
        bowler1_score = BowlerScores.objects.first()
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(9)
        bowler1_score.mark_bowl_score(1)
        bowler1_score.mark_bowl_score(1)

        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 11)
        self.assertEqual(is_valid, True)

    def test_spare_fill_ball_score(self):
        '''score after bowler throws a spare and strike in the final frame'''
        bowler1_score = BowlerScores.objects.first()
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(9)
        bowler1_score.mark_bowl_score(1)
        bowler1_score.mark_bowl_score(10)

        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 20)
        self.assertEqual(is_valid, True)

    def test_strike_fill_ball_score(self):
        '''score after bowler throws a strike in the final frame'''
        bowler1_score = BowlerScores.objects.first()
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(1)
        bowler1_score.mark_bowl_score(1)

        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 12)
        self.assertEqual(is_valid, True)

    def test_strike_fill_ball_more_strikes_score(self):
        '''score after bowler throws 3 strikes in the final frame'''
        bowler1_score = BowlerScores.objects.first()
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(0)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)

        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 30)
        self.assertEqual(is_valid, True)

    def test_walters_game_final_score(self):
        '''score after bowler throws about.com example game'''
        # http://bowling.about.com/od/rulesofthegame/a/bowlingscoring.htm
        bowler1_score = BowlerScores.objects.first()
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(7)
        bowler1_score.mark_bowl_score(3)
        bowler1_score.mark_bowl_score(7)
        bowler1_score.mark_bowl_score(2)
        bowler1_score.mark_bowl_score(9)
        bowler1_score.mark_bowl_score(1)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(2)
        bowler1_score.mark_bowl_score(3)
        bowler1_score.mark_bowl_score(6)
        bowler1_score.mark_bowl_score(4)
        bowler1_score.mark_bowl_score(7)
        bowler1_score.mark_bowl_score(3)
        bowler1_score.mark_bowl_score(3)

        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 168)
        self.assertEqual(is_valid, True)

    def test_perfect_game_final_score(self):
        '''score after bowler throws a perfect game'''
        bowler1_score = BowlerScores.objects.first()
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)
        bowler1_score.mark_bowl_score(10)

        score, is_valid = bowler1_score.get_score()
        self.assertEqual(score, 300)
        self.assertEqual(is_valid, True)
