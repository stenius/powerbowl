# Powerbowl

*Not fit for real world use.  Only a demo application.*

![](simpsons_bowling.jpg)

Powerbowl is an API service for scoring bowling games.  It is designed to take in pins knocked down for different players and to keep track of them while calculating and providing each player's score.

If you were in a bowling alley and looked at the display, it would be the service that powered the numbers behind the display.

## Usage:
  - Register and add users from api that represents what the computer terminal in front of lanes would use
    ##### POST /bowler/
    create a new bowler and get an bowler ID which can be used to create games in the response
    
    ```JSON
    {}
    ```
    ```JSON
    {
        "id": 2,
        "url": "http://localhost:8080/bowler/2/"
    }
    ```
    ##### POST /games/
    Create a new game with bowler:1 and bowler:2 and get the game id and initial score in the response
    ```JSON
    {
        "bowlers": [
			{
				"bowler": 1
			},
			{
				"bowler": 2
			}
		]
    }
    ```
    ```JSON
    {
        "id": 9,
        "url": "http://localhost:8080/games/9/",
        "bowlers": [
            {
                "bowler": 1,
                "score": 0,
                "score_is_valid": true,
                "frames": []
            },
            {
                "bowler": 2,
                "score": 0,
                "score_is_valid": true,
                "frames": []
            }
        ]
    }
    ```
  - Make API calls from a pinsetter machine which is able to inform Powerbowl how many pins were downed each bowl
    ##### POST /bowlerscores/1/bowl/add
    Set how many pins were knocked down for the bowler in a particular game
    ```JSON
    {
        "pins": 5
    }
    ```
  - Query API to get score information at any time
    ##### GET /games/3
    Get game score for all bowlers
    ```JSON
    {
        "id": 3,
        "url": "http://localhost:8080/games/3/",
        "bowlers": [
            {
                "bowler": 1,
                "score": 82,
                "score_is_valid": false,
                "frames": [
                    [
                        1,
                        9
                    ],
                    [
                        10,
                        null
                    ],
                    [
                        10,
                        null
                    ],
                    [
                        4,
                        5
                    ],
                    [
                        7,
                        3
                    ]
                ],
                "url": "http://localhost:8080/bowlerscores/1/"
            }
        ]
    }
    ```

### Version
0.0.1

### Tech

Powerbowl uses a number of open source projects to work properly:

* [Python] - Web framework for perfectionists with deadlines
* [Django] - Web framework for perfectionists with deadlines
* [PostgreSQL] - The world's most advanced open source database

And of course Powerbowl itself is open source with a [public repository][powerbowl]
 on GitLab.

### Installation

Powerbowl was designed to use [Python] 2.7.x or 3.4 to run.

Download and extract the [latest release][powerbowl].

Install the dependencies and start the development server.

```sh
$ cd powerbowl
$ pip install -r requirements.txt
$ python powerbowl/manage.py runserver
```

For production environments...

```sh
```
### Docker
Powerbowl is very easy to install and deploy in a Docker container.

By default, the Docker will expose port 80, so change this within the Dockerfile if necessary. When ready, simply use the Dockerfile to build the image.

```sh
cd powerbowl
fab build-docker
```
This will create the Powerbowl image and pull in the necessary dependencies. Look inside the `fabfile.py` and the `Dockerfile` for more details on how this works.

Once done, run the Docker image and map the port to whatever you wish on your host. In this example, we simply map port 8000 of the host to port 80 of the Docker (or whatever port was exposed in the Dockerfile):

```sh
docker run -d -p 8000:8080 --restart="always" powerbowl:latest
```

Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:8000
```


### Todos

 - Write API Code
 - Write API Tests
 - Write Docker Image Generation Continuous Integration Scripts

License
----

MIT




   [python]: <https://python.org>
   [powerbowl]: <https://gitlab.com/stenius/powerbowl>
   [django]: <https://django.com>
   [postgresql]: <https://www.postgresql.org>
